#lang scribble/manual

@require[scribble/extract
	(for-label uni-table/private/table-pict)]

@title{Pict Renderer}

@defmodule[uni-table/pict]

Use this module to output the result in DrRacket REPL or under similar
environment.

@include-extracted["../pict.rkt"]

@section{ECMA-48 SGR Pict}

@defmodule[uni-table/private/sgr-pict]

This module handles rendering strings with ECMA SGR sequences into a
picture. It is intended mainly for usage with DrRacket and for rendering
colored output to documentation.

@include-extracted["../private/sgr-pict.rkt"]

@section{Pict Conversion}

@defmodule[uni-table/private/table-pict]

This module performs all the transformation and prerendering like the printing
module but instead of producing output to a @racket[output-port?] or
@racket[string?], it creates a @racket[pict?] which can be displayed in
DrRacket or rendered into a scribble document.

@include-extracted["../private/table-pict.rkt"]
