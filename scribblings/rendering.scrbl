#lang scribble/manual

@require[scribble/extract]

@title{Rendering}

These modules handle the table contents and its borders rendering. They use the
table transformations to prepare the pre-rendered cells and then output the
cells interleaved with Unicode borders as required.



@section{Table Rendering}

@defmodule[uni-table/private/table-render]

This module implements the actual rendering of two-dimensional table - a list
of lists - into final multi-line string.

@include-extracted["../private/table-render.rkt"]



@section{Table Borders}

@defmodule[uni-table/private/table-borders]

This module implements rendering border bars between individual cells in a row
and between rows as well. It handles line style and weight merging and when to
include space for the border or make cells immediately adjacent.

@include-extracted["../private/table-borders.rkt"]



@section{SGR Formatting}

This module handles formatting blocks of texts parsed from CSI SGR
streams.

@defmodule[uni-table/private/sgr-format]
@include-extracted["../private/sgr-format.rkt"]
