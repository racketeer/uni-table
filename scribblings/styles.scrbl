#lang scribble/manual

@require[scribble/extract
	(for-label uni-table/private/table-render
		   racket/base)
	scribble/example
	racket/sandbox
	scribble/bnf
	]

@(define my-evaluator
   (parameterize ([sandbox-output 'string]
                  [sandbox-error-output 'string]
                  [sandbox-memory-limit 50])
     (make-evaluator 'racket/base
     		     #:requires '(uni-table/private/spec-syntax))))

@title{Styles}

All style categories share the same syntax and semantics of how they are
applied to the table cells. Currently there are three priority levels where
each successive level overrides whatever was set up in the previous
levels. The levels - from least important to most important - are:

@margin-note{
Support for setting style of individual cells will be added in some future
release!
}

@itemlist[
	@item{cell @racket[#:cell-] keywords, affects all cells in the table,}
	@item{column @racket[#:column-] and @racket[#:col-] keywords, affects given columns,}
	@item{row @racket[#:row-] keywords, affects particular rows.}]

The row settings therefore override everything else.

For borders, there is one special setting @racket[#:table-border] which affects
the outside borders of bordering cells of the table. The same effect can be
achieved with style templates, but it is a convenient feature to be able to
specify the table frame directly.

The style categories are:

@itemlist[
	@item{SGR style - terminal output attributes like color, intensity etc.,}
	@item{alignment - both vertical and horizontal alignment of text within the cell,}
	@item{border - cell borders for all its sides, thickness, line style and such.}]

Of course, any SGR sequences within the content of the cell (even at the very
beginning of it) override anything specified in the styles passed to
@racket[print-table]. The specified SGR style is just a
@racket[#:initial-state] for any SGR processing procedures used.



@section{Specification syntax}

@defmodule[uni-table/private/spec-syntax]

Minimalistic syntax to allow easy specification of fixed or variable
repetitions in style, alignment, or border specifications - both for rows and
columns. The specification language contains three types of tokens:

@itemlist[
	@item{style specification token - any @racket[list?],}
	@item{@racket[exact-positive-integer?] numbers before any specificaton token,}
	@item{the ellipsis @racket[...] after any token means the last token
	should be repeated if the whole specification needs to be extended to
	longer length than its head and tail expand to. Only one token of this
	type can be present.}]

Without actual style specifications, the syntax can be expanded directly:

@examples[#:eval my-evaluator
	#:label #f
(expand-spec '(2 (heavy) (light dashed) ... (light) (heavy)) 8)
(expand-spec '((light)) 8)
]

These expansions are used by @racket[table-transform] to make the rows' and the
columns' style specifications of the same length as the number of rows and
columns in transformed table.

@include-extracted["../private/spec-syntax.rkt"]



@section{SGR Styles}

@defmodule[uni-table/private/sgr-style]

This module implements a very small domain-specific language (DSL) for ECMA-48
SGR state notation. Full specification can be found in the source of
@racket[sgr-style-spec/c], which is a recursive @racket[flat-contract?]
implementing the following DSL:

@BNF[
  (list @nonterm{style} @BNF-seq[@nonterm{spec} @BNF-etc])
  (list @nonterm{spec}
  	@nonterm{ground}
	@nonterm{color}
	@nonterm{flag}
	@nonterm{intensity}
	@nonterm{underline})
  (list @nonterm{ground}
  	@BNF-seq[@litchar{#:background} @nonterm{color}]
  	@BNF-seq[@litchar{#:bg} @nonterm{color}]
  	@BNF-seq[@litchar{#:foreground} @nonterm{color}]
  	@BNF-seq[@litchar{#:fg} @nonterm{color}])
  (list @nonterm{color}
  	@elem{@racket[byte?]}
	@elem{color name accepted by @racket[sgr-color-name/c]}
	@BNF-seq[@litchar{(} @racket[exact-nonnegative-integer?] @elem{from @racketvalfont{#x000000} to @racketvalfont{#xffffff}} @litchar{)}]
	@BNF-seq[@litchar{(} @elem{@racket[byte?]} @elem{@racket[byte?]} @elem{@racket[byte?]} @litchar{)}])
  (list @nonterm{flag}
  	@litchar{italic}
	@litchar{blink}
	@litchar{reverse-video}
	@litchar{crossed-out}
	@litchar{strike-through})
  (list @nonterm{intensity}
  	@litchar{bold}
	@litchar{half-bright})
  (list @nonterm{underline}
  	@litchar{underline}
	@litchar{singly-underlined}
	@litchar{underlined}
	@litchar{single-underline}
	@litchar{doubly-underlined}
	@litchar{double-underline})
]

Usage examples:

@#reader scribble/comment-reader
(racketblock
; Red foreground, blue background, underlined italic font
(red italic #:background blue underline)

; RGB-specified red foreground, bold crossed-out font
(bold (255 0 0) strike-through)

; Bright yellow foreground, red background, doubly-underlined characters
(#:background red #:foreground bryellow doubly-underlined)
)

@include-extracted["../private/sgr-style.rkt"]



@section{Alignment}

@defmodule[uni-table/private/cell-align]

This module handles both vertical and horizontal alignment. The cell
alignment can be specified as a @racket[list?] of symbols which
represent valid vertical and horizontal alignments or overflow
settings. Only the last symbol for each property is taken into
account. In addition to these symbols a list containing a single
@racket[string?] will be used as line continuation for line clipping,
wrapping and reflow.

Valid horizontal alignments are:

@itemlist[
	@item{@racket[#f] - like @racket['left] but can be overridden,}
	@item{@racket['left],}
	@item{@racket['center], and}
	@item{@racket['right].}]

Their meaning is self-explanatory. Valid vertical alignments are:

@itemlist[
	@item{@racket[#f] - like @racket['top] but can be overridden,}
	@item{@racket['top],}
	@item{@racket['middle], and}
	@item{@racket['bottom].}]

Overflow settings are:

@itemlist[
	@item{@racket['clip] - clip to cell width,}
	@item{@racket['wrap] - wrap character by character to multiple lines of cell width, and}
	@item{@racket['reflow] - wrap to multiple lines of cell width re-flowing the text word by word.}
	]

When applying these to cells, they are parsed (for cells, rows and
columns) using @racket[parse-alignment-spec] and merged in the usual
priority like all style specifications using @racket[merge-alignments].

@include-extracted["../private/cell-align.rkt"]



@section{Cell Borders}

@defmodule[uni-table/private/cell-border]

This module implements notation for border lines decorating rectangular areas
like cells, rows, columns or whole tables. It contains both parsing
human-readable borders' specifications and procedures for merging adjacent and
overlaying inherited borders.

The border specification is as follows:

@BNF[
  (list @nonterm{style} @BNF-seq[@nonterm{spec} @BNF-etc])
  (list @nonterm{spec}
	@nonterm{side}
  	@nonterm{style})
  (list @nonterm{side}
  	@BNF-seq[@nonterm{name} @nonterm{style}]
	@BNF-seq[@nonterm{name} @litchar{(} @nonterm{style} @BNF-etc @litchar{)}])
  (list @nonterm{name}
  	@litchar{top}
	@litchar{left}
	@litchar{right}
	@litchar{bottom})
  (list @nonterm{style}
  	@nonterm{thickness}
	@nonterm{type})
  (list @nonterm{thickness}
  	@litchar{light}
	@litchar{heavy})
  (list @nonterm{type}
  	@litchar{solid}
	@litchar{dashed})]

These specifications can be parsed into actual @racket[borders?] struct using
@racket[borders-spec->borders]. The usual merging of cell, column and row
borders into final cell borders is done using the
@racket[merge-borders-overlay] procedure.

@include-extracted["../private/cell-border.rkt"]

