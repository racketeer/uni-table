#lang scribble/manual

@require[scribble/extract
	(for-label uni-table/private/table-cell
		   uni-table/private/table-structs)]

@title{Table Representation}

These modules contain the internal table representation, utilities to transform
any list of lists into it and some queries on these internal structures for
further use by the renderer.



@section{Table Structs}

@defmodule[uni-table/private/table-structs]

This module contains all internal @racket[struct]s describing table of
pre-rendered cells.

@include-extracted["../private/table-structs.rkt"]



@section{Table Queries}

@defmodule[uni-table/private/table-query]

This module implements simple queries on internal tables @racket[struct]s.

@include-extracted["../private/table-query.rkt"]



@section{Table Cell}

@defmodule[uni-table/private/table-cell]

This module handles direct manipulation of @racket[table-cell?] properties.

@include-extracted["../private/table-cell.rkt"]



@section{Transformations}

@defmodule[uni-table/private/table-transform]

This module implements transformations from @racket[table/c] to a well-formed
and pre-rendered @racket[table?] instances.

@include-extracted["../private/table-transform.rkt"]
