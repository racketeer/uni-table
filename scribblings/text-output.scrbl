#lang scribble/manual

@require[scribble/extract
	(for-label uni-table/private/ecma-csi)]

@title{Text Output}

These modules deal with the low-level terminal text output.

Any text, that is sent to the terminal is scanned for any control codes and
their sequences which may alter the visual properties of characted printed
later, move cursor on the screen, insert or erase characters, lines or whole
screen and many other actions given terminal supports. What remains after
processing these control codes and sequences is the text to be shown on the
screen.

The ECMA-48 standard specifies many control codes which can be divided - with
respect to this implementation - into the following categories:

@itemlist[
	@item{General control codes,}
	@item{Control Sequence Introducer (CSI), and}
	@item{CSI Select Graphic Rendition (SGR).}]

@margin-note{ Always remember that the CSI SGR processing does not perform any
actual character rendering and only allows for easier formatting of strings
with these sequences. In the end they are passed to the actual terminal the
program is running in. If in doubt, try your sequences without any processing
first! }

Only a very limited subset of the whole terminal capabilities as specified in
the ECMA-48 standard is supported by these modules. Anything non-CSI gets
passed through and any CSI except SGR gets filtered. All SGR codes are
processed and most of the standard - or commonly used - features are captured
and properly processed. Unknown parts of SGR sequences are discarded though.

The actual cell borders are drawn using Unicode character from the Box Drawing
Unicode block @racket[#,(racketvalfont "#x2500")]-@racket[#,(racketvalfont
"#x257F")]. All the possible combinations of light and heavy border junctions
are supported - there are 81 of them (including blank space). Because not all
combinations of double-line characters are present in Unicode, general
doubly-lined borders cannot be properly supported.



@section{ECMA-48 CSI}

@defmodule[uni-table/private/ecma-csi]

This module implements a subset of ECMA-48 CSI (Control Sequence Introducer)
decoding, allowing for capturing plain text and SGR (Select Graphic Rendition)
state snapshots anytime SGR control sequence is found in the text. Other
control sequences are discarded as they would interfere with table output.

The SGR module uses @racket[ecma-csi-tokenize] from this module to tokenize
input strings into a list of plain strings and string containing only the
control sequences starting with CSI. For further processing
@racket[ecma-csi-type] is used to determine which type of CSI sequence it is.

@include-extracted["../private/ecma-csi.rkt"]



@section{ECMA-48 SGR}

@defmodule[uni-table/private/ecma-sgr]

This module implements the internal SGR (Select Graphic Rendition) state
machine of ECMA-48 terminals. The following features are implemented:

@itemlist[
	@item{bold and half-bright intensity,}
	@item{italic font,}
	@item{single and double underline,}
	@item{text blinking,}
	@item{reverse video,}
	@item{strike-through (crossed-out) text, and}
	@item{foreground and background colors including
	@itemlist[
		@item{16-color,}
		@item{256-color,}
		@item{and 16M-color extensions.}
		]}
	]

Other features are not (yet) supported.

Use @racket[ecma-csi-sgr-parse-lines] to conver a string possibly containing
CSI SGR sequences into a list of @racket[sgr-list?]s - list of plain strings
and snapshots of @racket[sgr-state]s. Each line will become one
@racket[sgr-list?] in the resulting list.

Conversion from @racket[sgr-list?] to string with CSI sequences can be done
using @racket[sgr-list->csi-string]. The table cell renderer then uses
@racket[sgr-line-fill-block] to ensure the lists of @racket[sgr-list?] are
rectangular.

@include-extracted["../private/ecma-sgr.rkt"]

@section{Box Drawing}

@defmodule[uni-table/private/box-drawing]

This module implements two-dimensional orthogonal grid box drawing using two
possible line weights: light and heavy lines.

The Unicode Box Drawing Characters block contains all 80 combinations (81
including generic space character) for independently drawing no, light or heavy
line from the center of the character box in each of four major grid
directions.

As general support for double lines is not possible with current Unicode
specification, double lines box drawing is not implemented.

Straight lines can be solid or dashed. Lines in junctions are too short and
therefore can only be solid.

@include-extracted["../private/box-drawing.rkt"]
