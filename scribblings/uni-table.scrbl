#lang scribble/manual

@require[scribble/extract
	scribble/example
	racket/sandbox
	scribble/core
	scribble/html-properties
	racket/runtime-path
	uni-table/private/table-pict
	racket/string]

@(define my-evaluator
   (parameterize ([sandbox-output 'string]
                  [sandbox-error-output 'string]
                  [sandbox-memory-limit 50])
     (make-evaluator 'racket/base)))

@(define-runtime-path example-css "example.css")
@(define example-style
   (make-style "example"
               (list (make-css-addition example-css))))

@title{Unicode Tables}

@author[
	@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

@defmodule[uni-table]

This package is an easy-to-use terminal table renderer. It requires terminal
with UTF-8 support and can leverage ECMA-48 SGR control sequences for styling
both the table borders and cells' content.

The public API is inspired by @racket[tabular] from
@racketmodname[scribble/base] and of course @other-doc['(lib
"text-table/scribblings/text-table.scrbl") #:indirect
"text-table"]. Based on this prior work, the package aims to be usable
with the same expectations about argument names and their
behavior. However, some design decisions had to be made, which have
lead to slight differences.

Currently supported features are:

@itemlist[
	@item{Multi-line cell support with horizontal and vertical alignment specification,}
	@item{clipping cell contents to specified width or automatic character or word wrapping,}
	@item{merging borders, alignment and text styles based on row, column or table-global settings,}
	@item{foreground and background colors, single and double underlines, italic font, reverse video, blink, bold and half-bright intensity,}
	@item{thick (heavy) and thin (light) border lines, solid or dashed variants, co-existing in different cells of one table - @emph{
	double-line box drawing characters are neither supported, nor is such support
planned in the future,
	}}
	@item{separate border specification for the whole table (for convenience),}
	@item{row and column template styling with explicit and implicit repetitions, and}
	@item{optional explicit column width specification.}]

@nested-flow[example-style
(list
@examples[#:eval my-evaluator
	#:label (bold "Example usage:")
(require uni-table racket/string)
  (define lipsum
    (string-join
     (list
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit,"
      "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut"
      "enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi"
      "ut aliquip ex ea commodo consequat. Duis aute irure dolor in"
      "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla"
      "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in"
      "culpa qui officia deserunt mollit anim id est laborum.")
    " "))
  (define tbl
   (for/list ((y 4))
     (for/list ((x 4))
       (case (list x y)
         (((0 0)) "")
         (((0 1)) "Clip")
         (((0 2)) "Wrap")
         (((0 3)) "Reflow")
         (((1 0)) "Left")
         (((2 0)) "Right")
         (((3 0)) "Center")
         (else lipsum)))))
  (print-uni-table tbl
   #:col-widths '((8)(35))
   #:table-border '(heavy solid)
   #:col-align '((middle center)(left ("\t\\\\"))(right)(center))
   #:row-align '((center)(clip)(wrap)(reflow))
   #:col-borders '((solid heavy) (left light dashed))
   #:row-borders '((solid heavy) (top light dashed)))
])]

@(define lipsum
    (string-join
     (list
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit,"
      "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut"
      "enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi"
      "ut aliquip ex ea commodo consequat. Duis aute irure dolor in"
      "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla"
      "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in"
      "culpa qui officia deserunt mollit anim id est laborum.")
    " "))
@(define tbl
   (for/list ((y 4))
     (for/list ((x 4))
       (case (list x y)
         (((0 0)) "")
         (((0 1)) "Clip")
         (((0 2)) "Wrap")
         (((0 3)) "Reflow")
         (((1 0)) "Left")
         (((2 0)) "Right")
         (((3 0)) "Center")
         (else lipsum)))))
 
@nested[#:style 'inset
@bold{Colored example:}

@racketblock[
(print-uni-table tbl
                 #:col-borders '((solid heavy) (left light dashed))
                 #:row-borders '((solid heavy) (top light dashed))
                 #:row-style '((brred) () ... (White))
                 #:col-style '((brmagenta) ())
                 #:col-align '(4 (bottom) ())
                 #:border-style '(cyan)
		 #:col-widths '((8)(35))
   		 #:col-align '((middle center)(left ("\t\\\\"))(right)(center))
   		 #:row-align '((center)(clip)(wrap)(reflow))
                 #:table-border '(heavy solid))]

@nested{The output in the text terminal should look like this:}

@table->pict[tbl
		#:column-borders '((heavy solid)(left light dashed))
                #:row-borders '((heavy solid)(top light dashed))
                #:row-style '((brred) () ... (White))
                #:column-style '((brmagenta) ())
                #:border-style '(cyan #:bg black)
		#:column-widths '((8)(35))
   		#:row-align '((center)(clip)(wrap)(reflow))
		#:cell-style '(#:bg black white)
   		#:column-align '((middle center)(left ("\t\\\\"))(right)(center))
                #:table-border '(heavy solid)]
]

To see this colored example, run: racket -l uni-table

@include-extracted["../main.rkt"]

@include-section["text-output.scrbl"]

@include-section["styles.scrbl"]

@include-section["tables.scrbl"]

@include-section["rendering.scrbl"]

@include-section["pict.scrbl"]

